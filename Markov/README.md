# Domain Name generation through Markov Chains
The Conficker worm introduced a large number of tricks that allowed it to propogate.

One of these features was the ablility to pull updates from randomly generated domain names.
Just combating this feature alone required that ICANN place a bar on domain names generated.

The most concerning case though, was when a website name was generated that already exists as a
reputable site. This would force the repudability of the site to be evaluated. Letting a bad site
through would allow for vulnerable systems to be further compromised, but legit sights couldn't be
used to deconstruct the bot net because payloads are signed.

Within uniformly generated domain names, the chance of collision with a valid site is increadibly
small. When I brought this up during an iss meeting, the viability of alternative methods for
domain name generation was questioned.

This project aims to generate domain names using markov chains generated from the 500 most common
domain names. This greatly increases the likelyhood of collision with a valid site and would allow
for the creation of fake sites (before the release of the virus) whose made up names are believable.

Using markov chains also has the benefit of taking up far less space than more sophisticated
methods based on word lists.

## Example Output
```
❯ java markov
orkustotiglsches.de
tonyad.org
acliay.org
nhiaip.uk
s.ru
sachar.ru
exbe.ru
uselebbitu.com
oddicke.com
othbshomme.ru
eyphshonho.com
aon.de
x.com
smapombleresm.de
esta.uk
per.ru
thustelloosk.org
gsiliclleereyowotha.de
obacarschonarwe.com
dveelintranomevixfblel.net
vehinaletocaadimoumazndebesp.net
n.ru
goualere.net
grstb.com
tenyus.org
l.de
ogox.com
linse.net
nedonsebym.com
ingeba.com
ckenthitir.net
h.com
s.net
ac.uk
earec.ru
maspen.uk
ne.net
t.net
wadon.uk
y.net
jooo.com
meeaicodjourvea.de
phiegistizdebsogsgoogonglaiteshpoodinhele.ru
d.org
nssgysymonxst.de
e.ru
e.org
dridroumtvevengs.de
thna.com
te.com
```

