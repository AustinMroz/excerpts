import java.util.Scanner;
import java.io.File;
import java.util.Random;

public class markov {
   public static void main(String[] args) throws Exception {
      Random r = new Random();
      int[][] cm = new int[26][28];
      Scanner sc = new Scanner(new File("top500.csv"));
      while(sc.hasNextLine()) {
         String s = sc.nextLine();
         if(s.charAt(0)<'a' || s.charAt(0) >'z') 
            continue;
         int i;
         for(i=0;i<s.length() && s.charAt(i+1)!='.';i++) {
            if(s.charAt(i+1)<'a' || s.charAt(i+1) >'z') 
               break;
            cm[s.charAt(i)-'a'][s.charAt(i+1)-'a']++;
         }
         if(s.charAt(i)>='a' && s.charAt(i) <='z')
            cm[s.charAt(i)-'a'][26]++;
      }
      int tot=0;
      for(int a=0;a<26;a++) {
         int c=0;
         for(int b=0;b<27;b++) {
            c+=cm[a][b];
            //System.out.printf("%5d",cm[a][b]);
         }
         tot+=c;
         cm[a][27]=c;
         //System.out.println(" "+c);
      }
      int count=50;
      while(count-->0) {
         //choose starting letter
         int start = r.nextInt(tot);
         int i=0;
         while(start>0)
            start-=cm[i++][27];
         if(i!=0)
            i--;
         while(i!=26) {
            System.out.print((char)(i+'a'));
            start = r.nextInt(cm[i][27]);
            int nexti=0;
            while(start>0)
               start-=cm[i][nexti++];
            if(nexti!=0)
               nexti--;
            i = nexti;
         }
         String[] TLD = {".com",".org",".net",".ru",".de",".uk"};
         System.out.println(TLD[r.nextInt(6)]);
      }
   }
}
