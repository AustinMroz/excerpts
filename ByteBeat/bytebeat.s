	;; proper code to setup and end file should be added, of course
	;; t << >> p? 
	;; ^ & | %
	;; * / + -
	;; const ? array dup
	THUMB
	AREA	DATA, ALIGN=2
stack	SPACE 4*100		; no small stacks from me
prev	SPACE 4
	ALIGN
      AREA    |.text|, CODE, READONLY, ALIGN=2
	EXPORT ByteBeat
	EXPORT Delay
ByteBeat
	push {r3,r4,r5,lr}
	ldr r11, =stack
	;; actually execute commands
cloop	subs r2, #1
	blt finish
	ldmia r1!, {r3}
	cmp r3, #0
	bgt const
	;; non constant, begin switch code
	mvn r3,r3
	TBB[PC,r3]
BranchTB
	DCB ((CaseT - BranchTB)/2) ;really should confirm this
	DCB ((CaseLs - BranchTB)/2)
	DCB ((CaseRs - BranchTB)/2)
	DCB ((CasePr - BranchTB)/2)
	DCB ((CaseEx - BranchTB)/2)
	DCB ((CaseAn - BranchTB)/2)
	DCB ((CaseOr - BranchTB)/2)
	DCB ((CaseMo - BranchTB)/2)
	DCB ((CaseMu - BranchTB)/2)
	DCB ((CaseDi - BranchTB)/2)
	DCB ((CaseAd - BranchTB)/2)
	DCB ((CaseNe - BranchTB)/2)
	DCB ((CaseCo - BranchTB)/2) ;should never be called, but will likely be needed for bit alignment
	DCB ((CaseTe - BranchTB)/2)
	DCB ((CaseAr - BranchTB)/2)
	DCB ((CaseDu - BranchTB)/2)
CaseT	stmia r11!, {r0}	;or wherever I put t
	b cloop
CaseLs	ldmdb r11!, {r4,r5}
	lsl r4, r4, r5
	b str4
CaseRs	ldmdb r11!, {r4,r5}
	lsr r4, r4, r5
	b str4
CasePr	ldr r4, =prev
	ldr r4, [r4]
	b str4
CaseEx	ldmdb r11!, {r4,r5}
	eor r4, r4, r5
	b str4
CaseAn	ldmdb r11!, {r4,r5}
	and r4, r4, r5
	b str4
CaseOr	ldmdb r11!, {r4, r5}
	orr r4, r4, r5
	b str4
CaseMo	ldmdb r11!, {r4, r5}
	sdiv r3, r4, r5
	mls r4, r3, r5, r4
	b str4
CaseMu	ldmdb r11!, {r4, r5}
	mul r4, r4, r5
	b str4
CaseDi	ldmdb r11!, {r4, r5}
	sdiv r4, r4, r5
	b str4
CaseAd	ldmdb r11!, {r4, r5}
	add r4, r4, r5
	b str4
CaseNe	ldmdb r11!, {r4}
	mvn r4, r4
	add r4, r4, #1
	b str4
CaseCo	b cloop			;should be throwing errors here, oh vell
CaseTe	ldmdb r11!, {r3,r4,r5}
	cmp r3, #0
	bgt Tgr
	stmia r11!, {r5}
	b cloop
Tgr	stmia r11!, {r4}
	b cloop
CaseAr	ldmdb r11!, {r4}			;actually involved, we'll assume c code has sanitized
	ldmia r1!, {r5}
	udiv r3, r4, r5
	mls r4, r3, r5, r4
	ldr r4, [r1,r4, lsl #2]
	add r1, r1, r5, lsl #2
	b str4
CaseDu	ldr r4, [r11, #-4]
	b str4

str4	stmia r11!, {r4}
	b cloop

const	stmia r11!, {r3}
	b cloop

finish	ldr r11, =stack
	ldr r0, [r11]
	ldr r1, =prev
	str r0, [r1]
	pop {r3,r4,r5,PC}	;yata
	
Delay
	subs r0, #1
	bne.n Delay
	bx lr
	
	ALIGN
	END
