# ByteBeat
A bytebeat interpreter written in arm assembly for use on TI Launchpads.

In Introduction to Embeded Systems, 319k, we were tasked with designing an embeded system that would play back a simple song. The intent was that we have an array of notes, and possibly durations. I decided instead to design a program that would arbitrary sequences of RPN ByteBeat code.

In order to ensure the system remains realtime, the actual interpreting code was written in assembly and bakes heavy use of TableBranchByte. It is largely unsafe under the assumption that the stack limit could never be reached under normal conditions.

While the rest of the project code has been lost, it was largely dependent to the wiring used and of comparably minimal interest to the interpreter itself.

The assembly code was written for assembly with keil uvision which is not compatible with GAS.

## Commands
|Command | Index | Effect |
| ------ | ----- | ----------------------------------------------- |
| t      |  0    | Adds the current time counter to the stack      |
| <<     |  1    | Performs a left shift of x by y                 |
| >>     |  2    | Performs a right shift of x by y                |
| p      |  3    | Stores the previous result to the stack         |
| ^      |  4    | Performs the bitwise exor of x and y            |
| &      |  5    | Performs the bitwise and of x and y             |
| `|`    |  6    | Performs the bitwise or of x and y              |
| %      |  7    | Performs the modulus of x and y                 |
| *      |  8    | Multiplies the values of x and y                |
| /      |  9    | Divides x by y                                  |
| +      | 10    | Adds x and y                                    |
| -      | 11    | Negates the sign of x                           |
| const  | 12    | Filtered out in input code, performs NOP        |
| ?      | 13    | Ternerary operator, if x {y} else {z}           |
| array  | 14    | Next x values are an array to be indexed out of |
| dup    | 15    | Duplicate the last result                       |
