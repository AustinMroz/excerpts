//writes to the screen, currelty will give one pixel leeway, currently only accepts 40x40 oversampled images
//currently has some weird bug when a ship is right at the safe threshold on only the left side of the screen. Needs further testing
//above is likely due to bad bounds on window from adding extra pixels
void DrawAff(float the, float rad, const uint16_t in[]) {
  float co = cos(the);
  float si = sin(the);

	float scale = 128/rad;

	//maximum size of the image is the diagonal, rotated however far, and must be square, so we can simply do
	//a simplified affine transform on the top right corner to the maximum x and y offset for each side
	//Currently gives a pixel to everything which stacks with below, but seems needed
	int max =(int) (20*((co>0 ? co:-co)+(si>0 ? si:-si))/scale+3);

  //calculate max possible size and set window to minimize pushed pixels, This really makes me want to have doublebuffering
  setAddrWindow(64+co*rad-max,80-si*rad-max,64+co*rad+max-1,80-si*rad+max-1);//this is not that

  //begin pushing pixels, currently has purposeful buffer of size one
  for(int a=-max;a<max;a++)
    for(int b=-max;b<max;b++) {
      int c = (int)(b*co*scale-a*si*scale+.5f)+20;
      int r = (int)-(b*si*scale+a*co*scale+.5f)+20;
      if(c>2*matn-1 || c<0 || r>2*matm-1 || r<0) {
	      writedata((uint8_t)0);
				writedata((uint8_t)0);
			}
      else {
	      writedata((uint8_t)(in[r*40+c] >> 8));
				writedata((uint8_t)in[r*40+c]);
			}
    }
}
void DrawAff2(float the, float rad, float the2, const uint16_t in[]) {
  float co = cos(the2);
  float si = sin(the2);
	float co2 = cos(the);
	float si2 = sin(the);

	float scale = 128/rad;

	//maximum size of the image is the diagonal, rotated however far, and must be square, so we can simply do
	//a simplified affine transform on the top right corner to the maximum x and y offset for each side
	//Currently gives a pixel to everything which stacks with below, but seems needed
	int max =(int) (20*((co>0 ? co:-co)+(si>0 ? si:-si))/scale+1);

  //calculate max possible size and set window to minimize pushed pixels, This really makes me want to have doublebuffering
  setAddrWindow(64+co2*rad-max-1,80-si2*rad-max-1,64+co2*rad+max,80-si2*rad+max);//this is not that

  //begin pushing pixels, currently has purposeful buffer of size one
  for(int a=-max;a<max;a++)
    for(int b=-max;b<max;b++) {
      int c = (int)(b*co*scale-a*si*scale+.5f)+20;
      int r = (int)-(b*si*scale+a*co*scale+.5f)+20;
      if(c>2*matn-1 || c<0 || r>2*matm-1 || r<0) {
	      writedata((uint8_t)0);
				writedata((uint8_t)0);
			}
      else {
	      writedata((uint8_t)(in[r*40+c] >> 8));
				writedata((uint8_t)in[r*40+c]);
			}
    }
}
//purely for debugging and shouldn't be permanent
void ClearAff(float the, float rad) {
	float co = cos(the);
  float si = sin(the);
	float scale = 128/rad;
	
	//maximum size of the image is the diagonal, rotated however far, and must be square, so we can simply do
	//a simplified affine transform on the top right corner to the maximum x and y offset for each side
	//Currently gives a pixel to everything which stacks with below, but seems needed
	int max =(int) (20*((co>0 ? co:-co)+(si>0 ? si:-si))/scale+2);

  //calculate max possible size and set window to minimize pushed pixels, This really makes me want to have doublebuffering
  setAddrWindow(64+co*rad-max,80-si*rad-max,64+co*rad+max-1,80-si*rad+max-1);//this is not that
	
  //begin pushing pixels, currently has purposeful buffer of size one
  for(int a=-max;a<max;a++)
    for(int b=-max;b<max;b++) {
	      writedata((uint8_t)0);
				writedata((uint8_t)0);
    }
}
const uint16_t bulletsprite[] = {0,0,0,0,0,0x1F,0x1F,0,0,0x1F,0x1F,0,0,0,0,0};



struct Enemy {
  float r,t;
  uint32_t state;//contains times, index of patterns, enemy type, likely a seed of some sort for coordination with fleet
  float br,bt;//needed for non-lossy movement
};

struct Bullet {//still need to decide if bullets only fly out...
  float r,t;
};


void PortF_Init(void){ volatile uint32_t delay;
  SYSCTL_RCGCGPIO_R |= 0x00000020;  // 1) activate clock for Port F
  delay = SYSCTL_RCGCGPIO_R;        // allow time for clock to start
  GPIO_PORTF_LOCK_R = 0x4C4F434B;   // 2) unlock GPIO Port F
  GPIO_PORTF_CR_R = 0x1F;           // allow changes to PF4-0
  // only PF0 needs to be unlocked, other bits can't be locked
  GPIO_PORTF_AMSEL_R = 0x00;        // 3) disable analog on PF
  GPIO_PORTF_PCTL_R = 0x00000000;   // 4) PCTL GPIO on PF4-0
  GPIO_PORTF_DIR_R = 0x0E;          // 5) PF4,PF0 in, PF3-1 out
  GPIO_PORTF_AFSEL_R = 0x00;        // 6) disable alt funct on PF7-0
  GPIO_PORTF_PUR_R = 0x11;          // enable pull-up on PF0 and PF4
  GPIO_PORTF_DEN_R = 0x1F;          // 7) enable digital I/O on PF4-0
}

//Updates an enemy
//By far the most complex part of the game.
//Will require access to at least a global time variable in the near future
void UpdateEnemy(struct Enemy *e) {
  e->state++;
  e->r = e->br+10+15*cos((float)e->state/25);
  e->t = e->bt+(float)e->state/100;
  if(e->t>2*Pi)
    e->t-=2*Pi;
  else if(e->t<0)
    e->t+=2*Pi;
  //I'll probably need to convert to Cartesian and back which is sad.	
}

//proper enemy control code, state is unorthodox, but described as bellow
//least 10 bits are fixed point to ensure no loss of precision with ADC readings.
//next 9 bits are progress through current pattern. This means that at normal speed,
//each pattern takes about 8.5 seconds to complete. Faster patterns may use additional time to wait at base position.
//The next 4 bits will control pattern
//next bit reverses direction of above patterns. Note that above patterns are free to change directions, but this allows simultaneous waves with opposing direction
Void UpdateEnemy2(struct Enemy *e) {
  e->state+=(2048)-1024;//TODO: replace placeholder with adc value. Ideally read straight from fifo unless underflow returns 0
  switch((e->state>>19)&0xF) {
  case 1:
    e->r = e->br+15*(1-cos((float)((e->state>>10)&512)*2*Pi/512));//really need to optimize this, also probably add reverse as ternery...

  }
}

int main(void){
  TExaS_Init();  // set system clock to 80 MHz
  //Random_Init(1);//This game pretty much can't use RNG due to deterministic nature (save for noise generator)
  PortF_Init();
  ADC_Init();
  ADC_In();
  Output_Init();
  //ADC_In();
  Sound_Init();
  ST7735_FillScreen(0x0000);            // set screen to black
	
  //DrawAff(0, 64-10, check2); // old static player ship
  //DrawAff(3*Pi/2, 30, check2);


  //DrawAff(0, 10, enemy1);
  DrawAff(Pi/3, 10, enemy1);
  DrawAff(2*Pi/3, 10, enemy1);
  DrawAff(Pi, 10, enemy1);
  DrawAff(4*Pi/3, 10, enemy1);
  DrawAff(5*Pi/3, 10, enemy1);
  DrawAff(Pi/2,5, enemy1);
  //DrawAff(2*Pi/3, 20, enemy1);
  //DrawAff(Pi/3, 25, enemy1);
  //DrawAff(Pi/2, 30, enemy1);
  //DrawAff(2*Pi/3, 35, enemy1);
  struct Enemy enemies[6] = {{10,0,0,10,0},{10,Pi/6,(int)(100*Pi/6),10,Pi/6},{10,2*Pi/6,(int)(200*Pi/6),10,2*Pi/6},
                             {10,Pi/2,(int)(100*Pi/2),10,Pi/2},{10,4*Pi/6,(int)(400*Pi/6),10,4*Pi/6},{10,5*Pi/6,(int)(500*Pi/6),10,5*Pi/6}};
  struct Bullet friendly;
  float r=0;
  float t=0;
  NVIC_ST_CTRL_R = 0;
  NVIC_ST_RELOAD_R = 80000000/60;
  NVIC_ST_CURRENT_R = 0;
  NVIC_ST_CTRL_R = 1|(1<<2);
  while(t<1000000) {
    if(!(r&127)) {
      friendly.t = t;
      friendly.r = 64-11;
    }
    r++;
    if(friendly.r>0) {
      int16_t x = cos(friendly.t)*friendly.r;
      int16_t y = -sin(friendly.t)*friendly.r;
      ST7735_DrawBitmap(x+64-1, y+80-1, bulletsprite, 4, 4);//now asymmetrical, which I dislike, but this is probably for the best. Probably should switch so only enemy bullets are red.
      friendly.r--;
    }
    if((GPIO_PORTF_DATA_R&1)||(GPIO_PORTE_DATA_R&2)) {
      t+=(float)1/25;
      if(t>2*Pi)
	t-=2*Pi;
    }
      if((GPIO_PORTF_DATA_R&0x10)||(GPIO_PORTE_DATA_R&4)) {
      t-=1/25;
      if(t<0)
	2+=2*Pi;
    }
    DrawAff((float)t/25, 64-11, player1);
    for(int i=0;i<6;i++) {
      if(friendly.r<(enemies[i].r+2) && friendly.r>(enemies[i].r-2) && friendly.t>(enemies[i].t-1/8*Pi) && friendly.t<(enemies[i].t+1/8*Pi)) {//find efficient way to deal with near 0 edge case.
	GPIO_PORTF_DATA_R ^=8;
	friendly.r=0;
	enemies[i].r = 200;
      }
      UpdateEnemy(&enemies[i]);
      DrawAff(enemies[i].t,enemies[i].r,enemy1);
    }
    while(!(NVIC_ST_CTRL_R & (1<<16)));
  }
}
