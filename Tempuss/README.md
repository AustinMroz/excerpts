# Tempuss
The final project for Introduction to Embedded Systems, 319k, was to make a game.

I decided to make a game called Tempus. It heavily borrowed from the game Gyruss,
but featured entirely deterministic enemy controls. The player had full control over the time flow
of the game and could speed things up for greater challenge, or even reverse time when a mistake was
made.

The project itself was a work of beauty. It managed to squeeze a lot of amazing features including
music, sfx, and oversampled sprites into a measly 32KiB, but most of it has been lost.

I've provided a number of excerpts of what I could recover

## aff.c
Sections off the calls to draw entities to the screen and backend code I wrote to render entities
to the screen based on there theta and radius.

This code was pulled from various svn caches. It likely has mistakes that were later fixed.

# Tempuss.mp3
Part of the project was creating sfx and background music that could fit in the small storage space.

The background music was designed to scale up and down in tempo (but not pitch), based on the time
dilation of the game itself.

In order to accomplish this, I made bare bones implementation of a 2A03 and composed some music
that would be stylistically appropriate for this type of game.

The code I could find for the audio subsystem was mostly placeholder code from early on in
development, but I do have sampled audio from playing the song forwards at normal speed in FamiTracker.
