# Excerpts
A number of projects I've made over the years that do interesting stuff,
but are unable to be made into stand alone projects for one reason or another.

Many of these have missing code, are reliant upon embedded systems disassembled long ago,
or were too small to serve as a standalone repository
